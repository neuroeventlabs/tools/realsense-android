package com.neuroeventlabs.rscamera.toolbar;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.neuroeventlabs.rscamera.R;
import com.neuroeventlabs.rscamera.camera.CameraClientFragment;
import com.neuroeventlabs.rscamera.camera.CameraManager;
import com.neuroeventlabs.rscamera.camera.CameraManagerListener;
import com.neuroeventlabs.rscamera.camera.CameraManagerListenerAdapter;
import com.neuroeventlabs.rscamera.settings.SettingsManager;
import com.neuroeventlabs.rscamera.utils.Log;

public class ThumbnailFragment extends CameraClientFragment {

    private static final String TAG = "ThumbnailFragment";
    private static final int OPEN_FILE_MANAGER_REQ_CODE = 12345; // Own request code for identifying correct activity result

    private ImageView mThumbnailImageView;

    private CameraManagerListener mCameraManagerListener = new CameraManagerListenerAdapter() {

        @Override
        public void onRecordingThumbnailAvailable(String recordingFile, Bitmap bitmap) {
            super.onRecordingThumbnailAvailable(recordingFile, bitmap);
            Log.d(TAG, "onRecordingThumbnailAvailable " + recordingFile);
            mThumbnailImageView.setImageBitmap(bitmap);
            SettingsManager settingsManager = getSettingsManager();
            if (settingsManager != null) {
                settingsManager.storeThumbnail(recordingFile, bitmap);
            }
        }

        @Override
        public void onRecordingStarted() {
            super.onRecordingStarted();
            mThumbnailImageView.setVisibility(View.INVISIBLE);
        }

        @Override
        public void onRecordingEnded(boolean cameraShutdownRequested) {
            super.onRecordingEnded(cameraShutdownRequested);
            mThumbnailImageView.setVisibility(View.VISIBLE);
        }
    };

    public ThumbnailFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_thumbnail, container, false);
        mThumbnailImageView = v.findViewById(R.id.toolbar_thumbnail_image);
        mThumbnailImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "Thumbnail clicked");
                if (mThumbnailImageView.getVisibility() == View.VISIBLE) {
                    openGallery();
                }
            }
        });
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        mThumbnailImageView.setVisibility(View.VISIBLE);
        CameraManager cameraManager = getCameraManager();
        if (cameraManager != null) {
            cameraManager.registerListener(mCameraManagerListener);
        }
        SettingsManager settingsManager = getSettingsManager();
        if (settingsManager != null) {
            Bitmap thumbnailBitmap = settingsManager.getStoredThumbnail();
            if (thumbnailBitmap != null) {
                mThumbnailImageView.setImageBitmap(thumbnailBitmap);
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        CameraManager cameraManager = getCameraManager();
        if (cameraManager != null) {
            cameraManager.unregisterListener(mCameraManagerListener);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == OPEN_FILE_MANAGER_REQ_CODE) {
            if (data == null) {
                return;
            }
            Uri fileUri = data.getData();
            if (fileUri != null) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                intent.setDataAndTypeAndNormalize(fileUri, "video/*");
                startActivity(intent);
            }
        }
    }

    private void openGallery() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(intent, OPEN_FILE_MANAGER_REQ_CODE);
    }
}
