package com.neuroeventlabs.rscamera.toolbar;

import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.ImageViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.neuroeventlabs.rscamera.R;
import com.neuroeventlabs.rscamera.camera.CameraClientFragment;
import com.neuroeventlabs.rscamera.camera.CameraManager;
import com.neuroeventlabs.rscamera.camera.CameraManagerListener;
import com.neuroeventlabs.rscamera.camera.CameraManagerListenerAdapter;
import com.neuroeventlabs.rscamera.utils.Log;
import com.neuroeventlabs.rscamera.utils.PrefixDialog;

public class RecordButtonFragment extends CameraClientFragment {

    private static final String TAG = "RecordButtonFragment";

    private CameraManagerListener mCameraManagerListener = new CameraManagerListenerAdapter() {

        @Override
        public void onCameraAvailabilityChanged(boolean connected) {
            super.onCameraAvailabilityChanged(connected);
            mContainer.setVisibility(connected ? View.VISIBLE : View.INVISIBLE);
        }

        @Override
        public void onRecordingStarted() {
            super.onRecordingStarted();
            activateStopButton();
        }

        @Override
        public void onRecordingEnded(boolean cameraShutdownRequested) {
            super.onRecordingEnded(cameraShutdownRequested);
            activateRecordButton();
        }

    };

    private ImageView mButtonImageView;
    private FrameLayout mContainer;
    private boolean mButtonEnabled;

    public RecordButtonFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_record_button, container, false);
        mButtonImageView = v.findViewById(R.id.record_button_inner_icon);
        mContainer = v.findViewById(R.id.record_button_container);
        mContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mButtonEnabled) {
                    Log.d(TAG, "Button is disabled");
                    return;
                }
                                
                final CameraManager cm = getCameraManager();
                                
                if (cm != null) {
                    if (!cm.isRecordingActive()) {
                        startRecording(cm);
                    }
                    else {
                        Log.d(TAG, "Stopping recording");
                        cm.stopRecording(false);
                        toggleButtonEnabled(false);
                    }
                }
            }
        });
        return v;
    }

    private void startRecording(final CameraManager cm) {
        PrefixDialog prefixDialog = new PrefixDialog(getContext());
        String title = getString(R.string.dialog_title);
        String msg = getString(R.string.dialog_message);
        String okButtonText = getString(R.string.dialog_positive_button);
        String cancelButtonText = getString(R.string.dialog_negative_button);
        prefixDialog.showDialog(title, msg, okButtonText, cancelButtonText, new PrefixDialog.Listener() {
            @Override
            public void onOk(String prefix) {
                cm.setFilePrefix(prefix);
                Log.d(TAG, "Starting recording with file prefix " + prefix);
                cm.startRecording();
                toggleButtonEnabled(false);
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "Cancel clicked. Skipping recording.");
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        CameraManager cameraManager = getCameraManager();
        if (cameraManager != null) {
            cameraManager.registerListener(mCameraManagerListener);
            if (cameraManager.getConnectedDevicesCount() == 0) {
                mContainer.setVisibility(View.INVISIBLE);
                toggleButtonEnabled(false);
            }
            else {
                mContainer.setVisibility(View.VISIBLE);
                toggleButtonEnabled(true);
                if (cameraManager.isRecordingActive()) {
                    activateStopButton();
                }
                else {
                    activateRecordButton();
                }
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        CameraManager cameraManager = getCameraManager();
        if (cameraManager != null) {
            cameraManager.unregisterListener(mCameraManagerListener);
        }
    }

    private void activateStopButton() {
        toggleButtonEnabled(true);
        Context context = getContext();
        if (context != null) {
            int color = ContextCompat.getColor(context, R.color.colorRecordButtonRect);
            mButtonImageView.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.rectangle_drawable));
            ImageViewCompat.setImageTintList(mButtonImageView, ColorStateList.valueOf(color));
        }
    }

    private void activateRecordButton() {
        toggleButtonEnabled(true);
        Context context = getContext();
        if (context != null) {
            int color = ContextCompat.getColor(context, R.color.colorRecordButtonDot);
            mButtonImageView.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.circle_drawable));
            ImageViewCompat.setImageTintList(mButtonImageView, ColorStateList.valueOf(color));
        }
    }

    private void toggleButtonEnabled(boolean enabled) {
        Log.d(TAG, "toggleButtonEnabled " + enabled);
        mButtonEnabled = enabled;
        if (mButtonEnabled) {
            mButtonImageView.setAlpha(1.0f);
        }
        else {
            mButtonImageView.setAlpha(0.5f);
        }
    }

}
