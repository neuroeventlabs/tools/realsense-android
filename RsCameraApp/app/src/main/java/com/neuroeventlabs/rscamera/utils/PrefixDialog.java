package com.neuroeventlabs.rscamera.utils;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.neuroeventlabs.rscamera.R;
import com.neuroeventlabs.rscamera.camera.CameraManager;

public class PrefixDialog extends Dialog {

    public interface Listener {
        void onOk(String prefix);
        void onCancel();
    }

    private Context mContext;

    public PrefixDialog(Context context) {
        super(context);
        mContext = context;
    }

    public void showDialog(String title, String message, String okButtonText, String cancelButtonText, final Listener listener) {
        final Dialog dialog = new Dialog(mContext, R.style.PrefixDialogStyle);
        dialog.setContentView(R.layout.prefix_dialog);
        dialog.setCancelable(true);
        final EditText prefixEditText = dialog.findViewById(R.id.prefix_dialog_filename_prefix_edit_text);
        prefixEditText.setHint(CameraManager.DEFAULT_FILE_PREFIX);
        TextView titleTextView = dialog.findViewById(R.id.prefix_dialog_title_text);
        TextView bodyTextView = dialog.findViewById(R.id.prefix_dialog_body_text);
        Button okButton = dialog.findViewById(R.id.prefix_dialog_ok_button);
        Button cancelButton = dialog.findViewById(R.id.prefix_dialog_cancel_button);
        titleTextView.setText(title);
        bodyTextView.setText(message);
        okButton.setText(okButtonText);
        cancelButton.setText(cancelButtonText);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String customPrefix = prefixEditText.getText().toString();
                sendOkEvent(listener, customPrefix.length() > 0 ? customPrefix : CameraManager.DEFAULT_FILE_PREFIX);
                dialog.dismiss();
            }
        });
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendCancelEvent(listener);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void sendOkEvent(Listener listener, String prefix) {
        if (listener != null) {
            listener.onOk(prefix);
        }
    }

    private void sendCancelEvent(Listener listener) {
        if (listener != null) {
            listener.onCancel();
        }
    }
}
