package com.neuroeventlabs.rscamera.utils;

public class Log {

    public static void d(String tag, String msg) {
        android.util.Log.d(modifyTag(tag), msg);
    }

    public static void w(String tag, String msg) {
        android.util.Log.w(modifyTag(tag), msg);
    }

    public static void e(String tag, String msg) {  android.util.Log.e(modifyTag(tag), msg); }

    public static void e(String tag, String msg, Throwable ex){
        android.util.Log.e(modifyTag(tag), msg, ex);
    }

    public static void v(String tag, String msg) { android.util.Log.v(modifyTag(tag), msg); }

    public static void stacktrace(Throwable e) { e.printStackTrace(); }

    private static String modifyTag(String tag) {
        return String.format("RS-RECORDER-APP|%s", tag);
    }

}

