package com.neuroeventlabs.rscamera.camera;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Path;
import android.media.MediaFormat;
import android.media.MediaMuxer;
import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;

import com.intel.realsense.librealsense.CameraInfo;
import com.intel.realsense.librealsense.Config;
import com.intel.realsense.librealsense.Device;
import com.intel.realsense.librealsense.DeviceCallback;
import com.intel.realsense.librealsense.DeviceList;
import com.intel.realsense.librealsense.DeviceListener;
import com.intel.realsense.librealsense.Frame;
import com.intel.realsense.librealsense.FrameCallback;
import com.intel.realsense.librealsense.FrameSet;
import com.intel.realsense.librealsense.GLRendererNEL;
import com.intel.realsense.librealsense.Pipeline;
import com.intel.realsense.librealsense.RsContext;
import com.intel.realsense.librealsense.StreamFormat;
import com.intel.realsense.librealsense.StreamType;
import com.intel.realsense.librealsense.VideoFrame;
import com.neuroeventlabs.rscamera.utils.Log;
import com.neuroeventlabs.rscamera.utils.Toolbox;
import com.neuroeventlabs.rscamera.videorecorder.NELVideoRecorder;
import com.neuroeventlabs.rscamera.videorecorder.VideoFrameCollector;

import java.nio.ByteBuffer;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CopyOnWriteArrayList;

public class CameraManager {

    private static final String TAG = "CameraManager";

    private DeviceListener mDeviceListener = new DeviceListener() {
        @Override
        public void onDeviceAttach() {
            Log.d(TAG, "onDeviceAttach");
            sendCameraAvailabilityChanged(true);
        }

        @Override
        public void onDeviceDetach() {
            Log.d(TAG, "onDeviceDetach");
            sendCameraAvailabilityChanged(false);
            mCameraConnected = false;
            if (mRecordingActive) {
                stopRecording(true);
            }
        }
    };

    public interface PreviewListener {
        void onPreviewResetRequested();
        void onCameraPreviewFrame(Frame frame);
    }

    private RsContext mRsContext;

    public static final String DEFAULT_FILE_PREFIX          = "nel_video";

    public static final int DEFAULT_COLOR_STREAM_WIDTH      = 1280;
    public static final int DEFAULT_COLOR_STREAM_HEIGHT     = 720;
    public static final int DEFAULT_INFRA_STREAM_WIDTH      = 1280;
    public static final int DEFAULT_INFRA_STREAM_HEIGHT     = 720;
    public static final int COLOR_STREAM_COUNT              = 1;
    public static final int INFRA_STREAM_COUNT              = 2;

    private static final int DEFAULT_COLOR_VIDEO_WIDTH      = 720;
    private static final int DEFAULT_COLOR_VIDEO_HEIGHT     = 1280;
    private static final int DEFAULT_INFRA_VIDEO_WIDTH      = 1280;
    private static final int DEFAULT_INFRA_VIDEO_HEIGHT     = 1440;

    private static final int DEFAULT_VIDEO_BITRATE          = 7000000;
    private static final int DEFAULT_VIDEO_FRAME_RATE       = 30;
    private static final int ROTATE_COLOR_TRACK_DEGREES     = 90;
    private static final int ROTATE_INFRA_TRACK_DEGREES     = 0;
    private static final int FRAME_WAIT_TIMEOUT_MS          = 500;
    private static final int STREAM_FAILURE_INTERVAL_MS     = 5000;
    private static final int STREAM_FAILURE_MAX_COUNT       = 5;

    private static final int VIDEO_OUTPUT_FORMAT            = MediaMuxer.OutputFormat.MUXER_OUTPUT_MPEG_4;
    private static final String VIDEO_MIME_TYPE             = MediaFormat.MIMETYPE_VIDEO_AVC;

    private Context mContext;
    private Config mConfig;
    private Pipeline mPipeline;
    private boolean mPreviewActive;
    private boolean mRecordingActive;
    private boolean mCameraShutdownRequested;
    private boolean mCameraConnected;
    private boolean mInitialized;
    private VideoFrame mRecordingThumbnailFrame;
    private Timer mProgressTimer;
    private int mProgressSeconds;
    private List<CameraManagerListener> mListeners;
    private PreviewListener mPreviewListener;
    private Handler mMainHandler;
    private String mCurrentVideoRecordingFile;
    private String mFilePrefix;
    private long mFrameFeedFailureTimestamp;
    private int mFrameFeedFailureCounter;

    private NELVideoRecorder mVideoRecorder;

    private GLRendererNEL mColorVideoRenderer;
    private GLRendererNEL mInfraVideoRenderer;

    private NELVideoRecorder.Listener mVideoRecordListener = new NELVideoRecorder.Listener() {

        @Override
        public void onVideoFrameCollectorCreated(VideoFrameCollector videoFrameCollector, StreamType streamType) {
            Log.d(TAG, "onVideoFrameCollectorCreated " + streamType.name());
            switch (streamType) {
                case COLOR:
                    mColorVideoRenderer.setVideoFrameCollector(videoFrameCollector);
                    break;
                case INFRARED:
                    mInfraVideoRenderer.setVideoFrameCollector(videoFrameCollector);
                    break;
            }
        }

        @Override
        public void onVideoRecordingStarted(String file) {
            Log.d(TAG, "onVideoRecordingStarted");
            mRecordingActive = true;
            mRecordingThumbnailFrame = null;
            mCurrentVideoRecordingFile = file;
            sendRecordingStarted();
            Log.d(TAG, "Recording started: " + file);
        }

        @Override
        public void onVideoRecordingProgressStarted() {
            Log.d(TAG, "onVideoRecordingProgressStarted");
            startProgressTimer();
        }

        @Override
        public void onVideoRecordingProgressStopped() {
            Log.d(TAG, "onVideoRecordingProgressStopped");
            stopProgressTimer();
        }

        @Override
        public void onVideoRecordingStopped(NELVideoRecorder.VideoRecordingStatus status) {
            Log.d(TAG, "onVideoRecordingStopped " + status.name());
            mRecordingActive = false;
            stopProgressTimer();
            sendRecordingEnded();
            if (status == NELVideoRecorder.VideoRecordingStatus.STATUS_OK) {
                createThumbnailBitmap();
            }
            mCurrentVideoRecordingFile = null;
        }
    };

    public CameraManager(Context context) {
        RsContext.init(context);
        mContext = context;
        mInitialized = false;
        mListeners = new CopyOnWriteArrayList<>();
        mMainHandler = new Handler(Looper.getMainLooper());
    }

    public void init(PreviewListener previewListener, GLRendererNEL colorVideoRenderer, GLRendererNEL infraVideoRenderer) {
        Log.d(TAG, "init");
        if (mInitialized) {
            Log.d(TAG, "Already initialized");
            return;
        }
        mRsContext = new RsContext();
        mRsContext.setDevicesChangedCallback(mDeviceListener);
        mColorVideoRenderer = colorVideoRenderer;
        mInfraVideoRenderer = infraVideoRenderer;
        mVideoRecorder = new NELVideoRecorder(mContext, mVideoRecordListener);
        mPreviewListener = previewListener;
        mPipeline = new Pipeline();
        mConfig = new Config();
        mConfig.enableStream(StreamType.COLOR, DEFAULT_COLOR_STREAM_WIDTH, DEFAULT_COLOR_STREAM_HEIGHT, StreamFormat.RGBA8);
        mConfig.enableStream(StreamType.INFRARED, 1, DEFAULT_INFRA_STREAM_WIDTH, DEFAULT_INFRA_STREAM_HEIGHT, StreamFormat.ANY, DEFAULT_VIDEO_FRAME_RATE);
        mConfig.enableStream(StreamType.INFRARED, 2, DEFAULT_INFRA_STREAM_WIDTH, DEFAULT_INFRA_STREAM_HEIGHT, StreamFormat.ANY, DEFAULT_VIDEO_FRAME_RATE);
        mPreviewActive = false;
        mRecordingActive = false;
        mCameraShutdownRequested = false;
        mRecordingThumbnailFrame = null;
        mFilePrefix = DEFAULT_FILE_PREFIX;
        mFrameFeedFailureCounter = 0;
        mFrameFeedFailureTimestamp = 0;
        DeviceList deviceList = mRsContext.queryDevices();
        traceCameraInfo(deviceList);
        if (deviceList.getDeviceCount() > 0) {
            mCameraConnected = true;
            sendCameraAvailabilityChanged(true);
        }
        else {
            mCameraConnected = false;
        }

        mInitialized = true;
    }

    public void uninit() {
        Log.d(TAG, "uninit");
        if (!mInitialized) {
            Log.d(TAG, "Already uninitialized");
            return;
        }
        mVideoRecorder.uninit();
        mVideoRecorder = null;
        mPreviewListener = null;
        mDeviceListener = null;
        mPipeline = null;
        mConfig = null;
        mRsContext = null;
        mColorVideoRenderer = null;
        mInfraVideoRenderer = null;
        mListeners.clear();
        mInitialized = false;
        if (mRsContext != null) {
            mRsContext.close();
            mRsContext = null;
        }
    }

    public void registerListener(CameraManagerListener cameraManagerListener) {
        if (!mListeners.contains(cameraManagerListener)) {
            mListeners.add(cameraManagerListener);
        }
    }

    public void unregisterListener(CameraManagerListener cameraManagerListener) {
        mListeners.remove(cameraManagerListener);
    }


    public int getConnectedDevicesCount() {
        if (mRsContext != null) {
            DeviceList deviceList = mRsContext.queryDevices();
            return deviceList.getDeviceCount();
        }

        return 0;
    }

    public void start() {
        startPreview();
    }

    public void stop() {
        if (mRecordingActive) {
            stopRecording(true);
        }

        stopPreview();
    }

    public void setFilePrefix(String videoFilePrefix) {
        mFilePrefix = videoFilePrefix;
    }

    public void startRecording() {
        mVideoRecorder.init();
        mVideoRecorder.addVideoTrack(StreamType.COLOR, VIDEO_MIME_TYPE, 0, DEFAULT_COLOR_VIDEO_WIDTH, DEFAULT_COLOR_VIDEO_HEIGHT, DEFAULT_VIDEO_BITRATE, DEFAULT_VIDEO_FRAME_RATE, ROTATE_COLOR_TRACK_DEGREES, COLOR_STREAM_COUNT);
        mVideoRecorder.addVideoTrack(StreamType.INFRARED, VIDEO_MIME_TYPE, 1, DEFAULT_INFRA_VIDEO_WIDTH, DEFAULT_INFRA_VIDEO_HEIGHT, DEFAULT_VIDEO_BITRATE, DEFAULT_VIDEO_FRAME_RATE, ROTATE_INFRA_TRACK_DEGREES, INFRA_STREAM_COUNT);
        mVideoRecorder.start(mFilePrefix, VIDEO_OUTPUT_FORMAT);
    }

    public void stopRecording(boolean shutdownCamera) {
        mCameraShutdownRequested = shutdownCamera;
        mVideoRecorder.stop();
    }

    public boolean isRecordingActive() {
        return mRecordingActive;
    }

    private synchronized void startPreview() {

        if (mPreviewActive) {
            return;
        }

        try {
            Log.d(TAG, "Starting preview...");
            mPipeline.start(mConfig);
            mPreviewActive = true;
            mMainHandler.post(mFrameFeedRunnable);
            Log.d(TAG, "Preview started");
        }
        catch (Exception ex) {
            Log.e(TAG, "Failed to start preview", ex);
        }
    }

    private synchronized void stopPreview() {
        if (!mPreviewActive) {
            return;
        }
        try {
            Log.d(TAG, "Stopping preview...");
            mPreviewActive = false;
            mMainHandler.removeCallbacks(mFrameFeedRunnable);
            mPipeline.stop();
            Log.d(TAG, "Preview stopped");
        }
        catch (Exception e) {
            Log.d(TAG, "Failed to stop preview");
            mPipeline = null;
        }
    }

    private void handleColorFrame(VideoFrame frame, boolean showInViewfinder) {
        if (showInViewfinder && mPreviewListener != null) {
            mPreviewListener.onCameraPreviewFrame(frame);
        }

        if (mRecordingActive) {
            mColorVideoRenderer.upload(frame);
        }
    }

    private void handleInfraredFrame(VideoFrame frame, boolean showInViewfinder) {
        if (showInViewfinder && mPreviewListener != null) {
            mPreviewListener.onCameraPreviewFrame(frame);
        }

        if (mRecordingActive) {
            mInfraVideoRenderer.upload(frame);
        }
    }

    private void createThumbnailBitmap() {
        if (mRecordingThumbnailFrame != null) {
            int width = mRecordingThumbnailFrame.getWidth();
            int height = mRecordingThumbnailFrame.getHeight();
            int bitsPerPixel = mRecordingThumbnailFrame.getBitsPerPixel();

            byte[] imageBytes = new byte[width * height * bitsPerPixel / 8];
            mRecordingThumbnailFrame.getData(imageBytes);

            Bitmap tempBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

            ByteBuffer buffer = ByteBuffer.wrap(imageBytes);
            tempBitmap.copyPixelsFromBuffer(buffer);

            Matrix matrix = new Matrix();
            matrix.postRotate(90);

            Bitmap portraitBitmap = Bitmap.createBitmap(tempBitmap, 0, 0, tempBitmap.getWidth(), tempBitmap.getHeight(), matrix, true);

            width = portraitBitmap.getWidth();
            height = portraitBitmap.getHeight();

            Bitmap outputBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

            Path path = new Path();
            path.addCircle(width/2f, height/2f, width/2f, Path.Direction.CW);

            Canvas canvas = new Canvas(outputBitmap);
            canvas.clipPath(path);
            canvas.drawBitmap(portraitBitmap, 0, 0, null);

            sendRecordingThumbnailAvailable(mCurrentVideoRecordingFile, outputBitmap);
            tempBitmap.recycle();
            portraitBitmap.recycle();
        }
    }

    private FrameCallback mFrameCallback = new FrameCallback() {
        @Override
        public void onFrame(Frame frame) {
            VideoFrame videoFrame = (VideoFrame) frame;
            switch (frame.getProfile().getType()) {
                case COLOR:
                    if (mRecordingThumbnailFrame == null && mRecordingActive) {
                        mRecordingThumbnailFrame = (VideoFrame) videoFrame.clone();
                    }
                    handleColorFrame(videoFrame, true);
                    break;
                case INFRARED:
                    handleInfraredFrame(videoFrame, false);
                    break;
            }
        }
    };

    private Runnable mFrameFeedRunnable = new Runnable() {
        @Override
        public void run() {
            if (!mCameraConnected) {
                return;
            }
            try {
                try (FrameSet frames = mPipeline.waitForFrames(FRAME_WAIT_TIMEOUT_MS)) {
                    frames.foreach(mFrameCallback);
                    mMainHandler.post(mFrameFeedRunnable);
                }
            }
            catch (Exception ex) {
                handleFrameFeedFailure(ex);
            }
        }
    };

    private void handleFrameFeedFailure(Exception ex) {
        Log.e(TAG, "Failure in frame feed runnable", ex);
        long elapsedTimeSinceFirstFailure = 0;
        if (mFrameFeedFailureCounter == 0) {
            mFrameFeedFailureTimestamp = System.currentTimeMillis();
        }
        else {
            elapsedTimeSinceFirstFailure = System.currentTimeMillis() - mFrameFeedFailureTimestamp;
            if (elapsedTimeSinceFirstFailure > STREAM_FAILURE_INTERVAL_MS) {
                clearFrameFeedFailureCounter();
                mFrameFeedFailureTimestamp = System.currentTimeMillis();
            }
        }
        mFrameFeedFailureCounter++;
        Log.d(TAG, "Stream failure counter: " + mFrameFeedFailureCounter);
        if (isStreamFrozen(elapsedTimeSinceFirstFailure)) {
            if (mRecordingActive) {
                stopRecording(false);
                mMainHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (mPreviewListener != null) {
                            mPreviewListener.onPreviewResetRequested();
                        }
                    }
                }, 1000);
            }
            else {
                if (mPreviewListener != null) {
                    mPreviewListener.onPreviewResetRequested();
                }
            }
        }
        else {
            mMainHandler.post(mFrameFeedRunnable);
        }
    }

    private boolean isStreamFrozen(long elapsedTimeSinceFirstFailure) {
        if (mFrameFeedFailureCounter >= STREAM_FAILURE_MAX_COUNT && elapsedTimeSinceFirstFailure < STREAM_FAILURE_INTERVAL_MS) {
            Log.w(TAG, Toolbox.formatString("Stream seems frozen (%d failures within %d ms)", mFrameFeedFailureCounter, elapsedTimeSinceFirstFailure));
            clearFrameFeedFailureCounter();
            return true;
        }
        return false;
    }

    private void clearFrameFeedFailureCounter() {
        mFrameFeedFailureCounter = 0;
        mFrameFeedFailureTimestamp = 0;
    }

    private void startProgressTimer() {
        stopProgressTimer();
        mProgressSeconds = 0;
        mProgressTimer = new Timer();
        mProgressTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                Log.d(TAG, "Video recording progress " + mProgressSeconds);
                sendRecordingProgress(mProgressSeconds);
                mProgressSeconds++;
            }
        }, 0, 1000);
    }

    private void stopProgressTimer() {
        if (mProgressTimer != null) {
            mProgressTimer.cancel();
            mProgressTimer.purge();
            mProgressTimer = null;
        }
    }

    private void sendCameraAvailabilityChanged(final boolean cameraAvailable) {
        mMainHandler.post(new Runnable() {
            @Override
            public void run() {
                for (CameraManagerListener listener : mListeners) {
                    if (listener != null) {
                        listener.onCameraAvailabilityChanged(cameraAvailable);
                    }
                }
            }
        });
    }

    private void sendRecordingStarted() {
        mMainHandler.post(new Runnable() {
            @Override
            public void run() {
                for (CameraManagerListener listener : mListeners) {
                    if (listener != null) {
                        listener.onRecordingStarted();
                    }
                }
            }
        });
    }

    private void sendRecordingProgress(final int progressSeconds) {
        mMainHandler.post(new Runnable() {
            @Override
            public void run() {
                for (CameraManagerListener listener : mListeners) {
                    if (listener != null) {
                        listener.onRecordingProgress(progressSeconds);
                    }
                }
            }
        });
    }

    private void sendRecordingEnded() {
        mMainHandler.post(new Runnable() {
            @Override
            public void run() {
                for (CameraManagerListener listener : mListeners) {
                    if (listener != null) {
                        listener.onRecordingEnded(mCameraShutdownRequested);
                    }
                }
            }
        });
    }

    private void sendRecordingThumbnailAvailable(final String recordingFile, final Bitmap thumbnail) {
        mMainHandler.post(new Runnable() {
            @Override
            public void run() {
                for (CameraManagerListener listener : mListeners) {
                    if (listener != null) {
                        listener.onRecordingThumbnailAvailable(recordingFile, thumbnail);
                    }
                }
            }
        });
    }

    private void traceCameraInfo(DeviceList deviceList) {
        try {
            deviceList.foreach(new DeviceCallback() {
                @Override
                public void onDevice(Device device) {
                    String message = Toolbox.formatString("%s FW Version %s", device.getInfo(CameraInfo.NAME), device.getInfo(CameraInfo.FIRMWARE_VERSION));
                    Log.d(TAG, message);
                    Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
                }
            });
        }
        catch (Exception ex) {
            Log.e(TAG, "Failure in traceCameraInfo", ex);
        }
    }

}
