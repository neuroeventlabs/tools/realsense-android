package com.neuroeventlabs.rscamera;

import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.neuroeventlabs.rscamera.utils.Toolbox;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractActivity extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback {

    private static final String TAG = "AbstractActivity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        checkPermissions();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

    private void checkPermissions() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getApplicationContext().getPackageName(), PackageManager.GET_PERMISSIONS);
            List<String> missingPermissions = new ArrayList<>();
            for (String permission : info.requestedPermissions) {
                int permissionCheck = ContextCompat.checkSelfPermission(this, permission);
                if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                    Log.w(TAG, Toolbox.formatString("Permission '%s' not granted.", permission));
                    missingPermissions.add(permission);
                }
                else {
                    Log.d(TAG, Toolbox.formatString("Permission '%s' already granted.", permission));
                }
            }
            if (missingPermissions.size() > 0) {
                int permissionRequestId = 1000;
                ActivityCompat.requestPermissions(this, missingPermissions.toArray(new String[missingPermissions.size()]), permissionRequestId);
            }
        }
        catch (Exception ex) {
            Log.e(TAG, "Failure in checkPermissions", ex);
        }
    }

    @Override
    public void onRequestPermissionsResult(int permissionRequestId, @NonNull String permissions[], @NonNull int[] grantResults) {
        Log.d(TAG, Toolbox.formatString("onRequestPermissionsResult permissions.length=%d, grantResults.length=%d", permissions.length, grantResults.length));
        // If request is cancelled, the result arrays are empty.
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "Permissions granted!");
        }
        else {
            Log.w(TAG, "Permissions not granted!");
        }
    }

}
