package com.neuroeventlabs.rscamera;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;

import com.intel.realsense.librealsense.Frame;
import com.intel.realsense.librealsense.GLRsSurfaceViewNEL;
import com.neuroeventlabs.rscamera.camera.CameraClient;
import com.neuroeventlabs.rscamera.camera.CameraManager;
import com.neuroeventlabs.rscamera.camera.CameraManagerListener;
import com.neuroeventlabs.rscamera.camera.CameraManagerListenerAdapter;
import com.neuroeventlabs.rscamera.settings.SettingsManager;
import com.neuroeventlabs.rscamera.utils.Toolbox;

public class MainActivity extends AbstractActivity implements CameraClient {

    private static final String TAG = "MainActivity";
    private static final int CRITICAL_BATTERY_PERCENTAGE = 10;
    private CameraManager mCameraManager;
    private FrameLayout mCameraErrorScreen;
    private GLRsSurfaceViewNEL mViewfinder;
    private GLRsSurfaceViewNEL mColorVideoView;
    private GLRsSurfaceViewNEL mInfraVideoView;
    private SettingsManager mSettingsManager;

    private BroadcastReceiver mBatteryStatusReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null && intent.getAction() != null) {
                int status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
                int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
                int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, 100);
                int plugType = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);

                switch (intent.getAction()) {
                    case Intent.ACTION_BATTERY_CHANGED:
                        Log.d(TAG, Toolbox.formatString("Battery level changed (status=%d, level=%d, scale=%d, plugType=%d)", status, level, scale, plugType));
                        if (level < CRITICAL_BATTERY_PERCENTAGE) {
                            if (mCameraManager != null && mCameraManager.isRecordingActive()) {
                                Log.d(TAG, "Stopping recording due to low battery level.");
                                mCameraManager.stopRecording(false);
                            }
                        }
                        break;
                    case Intent.ACTION_BATTERY_LOW:
                        Log.d(TAG, "Battery low.");
                        break;
                }
            }
        }
    };

    private CameraManagerListener mCameraManagerListener = new CameraManagerListenerAdapter() {
        @Override
        public void onCameraAvailabilityChanged(boolean connected) {
            Log.d(TAG, "onCameraAvailabilityChanged " + connected);
            if (!connected) {
                mCameraErrorScreen.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onRecordingEnded(boolean cameraShutdownRequested) {
            super.onRecordingEnded(cameraShutdownRequested);
            Log.d(TAG, "onRecordingEnded " + cameraShutdownRequested);
            if (cameraShutdownRequested) {
                shutdownCamera();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        setContentView(R.layout.activity_main);
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_BATTERY_CHANGED);
        filter.addAction(Intent.ACTION_BATTERY_LOW);
        registerReceiver(mBatteryStatusReceiver, new IntentFilter(filter));
        mViewfinder = findViewById(R.id.main_viewfinder);
        mViewfinder.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        setupColorVideoRecordingSurface();
        setupInfraVideoRecordingSurface();
        mSettingsManager = new SettingsManager(getApplicationContext());
        mCameraManager = new CameraManager(getApplicationContext());
        mCameraErrorScreen = findViewById(R.id.no_camera_error_screen);
    }

    private void setupColorVideoRecordingSurface() {
        mColorVideoView = findViewById(R.id.main_video_view_color);
        mColorVideoView.setRecordingEnabled(true);
        mColorVideoView.setStreamCount(1);
        mColorVideoView.getLayoutParams().width = CameraManager.DEFAULT_COLOR_STREAM_WIDTH;
        mColorVideoView.getLayoutParams().height = CameraManager.DEFAULT_COLOR_STREAM_HEIGHT * CameraManager.COLOR_STREAM_COUNT;
    }

    private void setupInfraVideoRecordingSurface() {
        mInfraVideoView = findViewById(R.id.main_video_view_infra);
        mInfraVideoView.setRecordingEnabled(true);
        mInfraVideoView.setStreamCount(2);
        mInfraVideoView.getLayoutParams().width = CameraManager.DEFAULT_INFRA_STREAM_WIDTH;
        mInfraVideoView.getLayoutParams().height = CameraManager.DEFAULT_INFRA_STREAM_HEIGHT * CameraManager.INFRA_STREAM_COUNT;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mBatteryStatusReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
        mColorVideoView.clear();
        mInfraVideoView.clear();
        mCameraManager.registerListener(mCameraManagerListener);
        mCameraManager.init(new CameraManager.PreviewListener() {
            @Override
            public void onPreviewResetRequested() {
                recreate();
            }

            @Override
            public void onCameraPreviewFrame(Frame frame) {
                if (mViewfinder != null) {
                    mViewfinder.upload(frame);
                }
            }
        }, mColorVideoView.getRenderer(), mInfraVideoView.getRenderer());

        if (mCameraManager.getConnectedDevicesCount() > 0) {
            mViewfinder.clear();
            mCameraManager.start();
            mCameraErrorScreen.setVisibility(View.GONE);
        }
        else {
            mCameraErrorScreen.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
        if (mCameraManager.isRecordingActive()) {
            mCameraManager.stopRecording(true);
        }
        else {
            shutdownCamera();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        Log.d(TAG, "onSaveInstanceState");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop");
    }

    @Override
    public CameraManager getCameraManager() {
        return mCameraManager;
    }

    @Override
    public SettingsManager getSettingsManager() { return mSettingsManager; }

    private void shutdownCamera() {
        Log.d(TAG, "shutdownCamera");
        if (mCameraManager != null) {
            if (mCameraManager.getConnectedDevicesCount() > 0) {
                mCameraManager.stop();
            }
            mCameraManager.unregisterListener(mCameraManagerListener);
            mCameraManager.uninit();
        }
    }

}
