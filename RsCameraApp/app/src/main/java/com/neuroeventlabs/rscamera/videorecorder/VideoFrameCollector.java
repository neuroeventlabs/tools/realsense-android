package com.neuroeventlabs.rscamera.videorecorder;

import com.intel.realsense.librealsense.GLVideoFrameNEL;

public interface VideoFrameCollector {
    void onVideoFrameAvailable(GLVideoFrameNEL videoFrame, double timeStamp, int index);
    boolean isFrameCollectionEnabled();

}
