package com.neuroeventlabs.rscamera.settings;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.preference.PreferenceManager;

import com.neuroeventlabs.rscamera.utils.Log;
import com.neuroeventlabs.rscamera.utils.Toolbox;

import java.io.File;
import java.io.FileOutputStream;

public class SettingsManager {

    private static final String TAG = "SettingsManager";
    private static final String LATEST_VIDEO_PATH_KEY = "LATEST_VIDEO_PATH";
    private SharedPreferences mSharedPreferences;
    private String mLatestThumbnailPath;

    public SettingsManager(Context context) {
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        mLatestThumbnailPath = Toolbox.formatString("%s%sthumbnail.png", context.getFilesDir().getAbsolutePath(), File.separator);
    }

    public void storeThumbnail(String videoPath, Bitmap bitmap) {
        Log.d(TAG, "storeThumbnail");
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(LATEST_VIDEO_PATH_KEY, videoPath);
        saveThumbnail(bitmap);
        editor.apply();
    }

    public Bitmap getStoredThumbnail() {
        Log.d(TAG, "getStoredThumbnail");
        String videoPath = mSharedPreferences.getString(LATEST_VIDEO_PATH_KEY, null);
        if (videoPath != null && Toolbox.fileExists(videoPath)) {
            return readThumbnail();
        }
        return null;
    }

    private Bitmap readThumbnail() {
        Log.d(TAG, "readThumbnail");
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            options.inSampleSize = 2;
            return BitmapFactory.decodeFile(mLatestThumbnailPath, options);
        }
        catch (Exception ex) {
            Log.e(TAG, "Failure in save thumbnail", ex);
            return null;
        }
    }

    private void saveThumbnail(Bitmap bitmap) {
        Log.d(TAG, "saveThumbnail");
        try (FileOutputStream out = new FileOutputStream(mLatestThumbnailPath)) {
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
        }
        catch (Exception ex) {
            Log.e(TAG, "Failure in save thumbnail", ex);
        }
    }
}
