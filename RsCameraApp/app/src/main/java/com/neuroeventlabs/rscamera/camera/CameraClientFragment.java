package com.neuroeventlabs.rscamera.camera;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.neuroeventlabs.rscamera.settings.SettingsManager;

public abstract class CameraClientFragment extends Fragment {

    private CameraClient mCameraClient;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCameraClient = (CameraClient) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCameraClient = null;
    }

    protected CameraManager getCameraManager() {
        if (mCameraClient != null && mCameraClient.getCameraManager() != null) {
            return mCameraClient.getCameraManager();
        }
        else {
            return null;
        }
    }

    protected SettingsManager getSettingsManager() {
        if (mCameraClient != null && mCameraClient.getSettingsManager() != null) {
            return mCameraClient.getSettingsManager();
        }
        else {
            return null;
        }
    }

}
