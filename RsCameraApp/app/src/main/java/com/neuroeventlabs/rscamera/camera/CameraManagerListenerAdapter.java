package com.neuroeventlabs.rscamera.camera;

import android.graphics.Bitmap;

public class CameraManagerListenerAdapter implements CameraManagerListener {

    @Override
    public void onCameraAvailabilityChanged(boolean connected) { }

    @Override
    public void onRecordingStarted() { }

    @Override
    public void onRecordingProgress(int seconds) { }

    @Override
    public void onRecordingEnded(boolean cameraShutdownRequested) { }

    @Override
    public void onRecordingThumbnailAvailable(String recordingFile, Bitmap bitmap) { }

}
