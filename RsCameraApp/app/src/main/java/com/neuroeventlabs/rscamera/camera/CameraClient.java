package com.neuroeventlabs.rscamera.camera;

import com.neuroeventlabs.rscamera.settings.SettingsManager;

public interface CameraClient {
    CameraManager getCameraManager();
    SettingsManager getSettingsManager();
}
