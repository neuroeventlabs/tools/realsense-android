package com.neuroeventlabs.rscamera.toolbar;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.neuroeventlabs.rscamera.R;
import com.neuroeventlabs.rscamera.camera.CameraClientFragment;

public class RecordingToolbarFragment extends CameraClientFragment {

    public RecordingToolbarFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_recording_toolbar, container, false);
    }

}
