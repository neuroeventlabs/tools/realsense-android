package com.neuroeventlabs.rscamera.videorecorder;

import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaFormat;
import android.util.Log;

import com.neuroeventlabs.rscamera.utils.Toolbox;

import java.nio.ByteBuffer;

class NELVideoEncoder {

    private static final int I_FRAME_INTERVAL = 5;
    private String mLogTag;
    private MediaCodec mMediaCodec;
    private NELMediaMuxer mMediaMuxer;
    private MediaCodec.BufferInfo mBufferInfo;

    private int mTrackIndex;
    private String mTrackIdentifier;

    NELVideoEncoder(NELMediaMuxer mediaMuxer, String id, String mimeType, int trackIndex, int bitRate, int frameRate, int width, int height) {
        mLogTag = Toolbox.formatString("NELVideoEncoder_%s", id);

        try {
            mMediaMuxer = mediaMuxer;
            mTrackIndex = trackIndex;
            mTrackIdentifier = id;
            MediaFormat format = MediaFormat.createVideoFormat(mimeType, width, height);
            format.setInteger(MediaFormat.KEY_COLOR_FORMAT, MediaCodecInfo.CodecCapabilities.COLOR_FormatSurface);
            format.setInteger(MediaFormat.KEY_BIT_RATE, bitRate);
            format.setInteger(MediaFormat.KEY_FRAME_RATE, frameRate);
            format.setInteger(MediaFormat.KEY_I_FRAME_INTERVAL, I_FRAME_INTERVAL);
            mMediaCodec = MediaCodec.createEncoderByType(mimeType);
            mMediaCodec.configure(format, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);
            mBufferInfo = new MediaCodec.BufferInfo();
            Log.d(mLogTag, Toolbox.formatString("Created video encoder %s", id));
        }
        catch (Exception ex) {
            Log.e(mLogTag, "Failed to create video encoder", ex);
        }
    }

    MediaCodec getMediaCodec() {
        return mMediaCodec;
    }

    void drain(boolean endOfStream) {
        if (endOfStream) {
            mMediaCodec.signalEndOfInputStream();
        }

        while (true) {
            int bufferIndex = mMediaCodec.dequeueOutputBuffer(mBufferInfo, 10000);
            if (bufferIndex == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
                mMediaMuxer.addTrack(mTrackIndex, mTrackIdentifier, mMediaCodec.getOutputFormat());
            }
            else if (bufferIndex == MediaCodec.INFO_TRY_AGAIN_LATER) {
                if (!endOfStream) {
                    break;
                }
            }
            else if (bufferIndex < 0) {
                Log.d(mLogTag, "bufferIndex < 0");
            }
            else {
                writeSampleData(bufferIndex);
                if ((mBufferInfo.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0) {
                    break;
                }
            }
        }
    }

    void release() {
        if (mMediaCodec != null) {
            try {
                Log.d(mLogTag, "Stopping encoder");
                mMediaCodec.stop();
            }
            catch (Exception ex) {
                Log.e(mLogTag, "Failed to stop encoder", ex);
            }
            finally {
                mMediaCodec.release();
                mMediaMuxer = null;
                mBufferInfo = null;
            }
        }
    }

    private void writeSampleData(int bufferIndex) {
        ByteBuffer encodedData = mMediaCodec.getOutputBuffer(bufferIndex);
        if (encodedData != null) {
            if ((mBufferInfo.flags & MediaCodec.BUFFER_FLAG_CODEC_CONFIG) != 0) {
                mBufferInfo.size = 0;
                return;
            }
            if (mBufferInfo.size != 0) {
                mMediaMuxer.writeSampleData(mTrackIndex, encodedData, mBufferInfo);
            }
        }
        mMediaCodec.releaseOutputBuffer(bufferIndex, false);
    }

}
