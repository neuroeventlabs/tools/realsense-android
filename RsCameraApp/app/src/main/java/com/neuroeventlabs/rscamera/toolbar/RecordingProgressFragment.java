package com.neuroeventlabs.rscamera.toolbar;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.neuroeventlabs.rscamera.R;
import com.neuroeventlabs.rscamera.camera.CameraClientFragment;
import com.neuroeventlabs.rscamera.camera.CameraManager;
import com.neuroeventlabs.rscamera.camera.CameraManagerListener;
import com.neuroeventlabs.rscamera.camera.CameraManagerListenerAdapter;
import com.neuroeventlabs.rscamera.utils.Log;
import com.neuroeventlabs.rscamera.utils.Toolbox;

public class RecordingProgressFragment extends CameraClientFragment {

    private static final String TAG = "RecordingProgressFragment";
    private static final String PROGRESS_TIME_FORMAT = "%02d:%02d:%02d";

    private TextView mRecordProgressText;
    private ImageView mRecordingDot;

    private CameraManagerListener mCameraManagerListener = new CameraManagerListenerAdapter() {

        @Override
        public void onCameraAvailabilityChanged(boolean connected) {
            super.onCameraAvailabilityChanged(connected);
            if (connected) {
                mRecordProgressText.setText("");
                mRecordingDot.setVisibility(View.INVISIBLE);
            }
        }

        @Override
        public void onRecordingStarted() {
            super.onRecordingStarted();
            mRecordProgressText.setText(Toolbox.formatString(PROGRESS_TIME_FORMAT, 0, 0, 0));
            mRecordingDot.setVisibility(View.VISIBLE);
        }

        @Override
        public void onRecordingProgress(int seconds) {
            super.onRecordingProgress(seconds);
            int hours = seconds / 3600;
            int minutes = (seconds % 3600) / 60;
            int sec = seconds % 60;
            String recordingText = Toolbox.formatString(PROGRESS_TIME_FORMAT, hours, minutes, sec);
            Log.d(TAG, "onRecordingProgress " + recordingText);
            mRecordingDot.setVisibility(seconds % 2 == 0 ? View.VISIBLE : View.INVISIBLE);
            mRecordProgressText.setText(recordingText);
        }

        @Override
        public void onRecordingEnded(boolean cameraShutdownRequested) {
            super.onRecordingEnded(cameraShutdownRequested);
            mRecordingDot.setVisibility(View.INVISIBLE);
        }
    };

    public RecordingProgressFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_recording_progress, container, false);
        mRecordProgressText = v.findViewById(R.id.record_progress_text);
        mRecordProgressText.setText("");
        mRecordingDot = v.findViewById(R.id.record_progress_dot);
        mRecordingDot.setVisibility(View.INVISIBLE);
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        CameraManager cameraManager = getCameraManager();
        if (cameraManager != null) {
            cameraManager.registerListener(mCameraManagerListener);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        CameraManager cameraManager = getCameraManager();
        if (cameraManager != null) {
            cameraManager.unregisterListener(mCameraManagerListener);
        }
    }

}
