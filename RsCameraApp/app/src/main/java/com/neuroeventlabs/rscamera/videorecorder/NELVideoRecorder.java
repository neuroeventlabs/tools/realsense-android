package com.neuroeventlabs.rscamera.videorecorder;

import android.content.Context;
import android.media.MediaMuxer;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.intel.realsense.librealsense.StreamType;
import com.neuroeventlabs.rscamera.BuildConfig;
import com.neuroeventlabs.rscamera.utils.Toolbox;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.concurrent.CountDownLatch;

public class NELVideoRecorder {

    private static final String TAG = "NELVideoRecorder";

    public interface Listener {
        void onVideoFrameCollectorCreated(VideoFrameCollector videoFrameCollector, StreamType streamType);
        void onVideoRecordingStarted(String file);
        void onVideoRecordingProgressStarted();
        void onVideoRecordingProgressStopped();
        void onVideoRecordingStopped(VideoRecordingStatus status);
    }

    public enum VideoRecordingStatus {
        STATUS_OK,
        STATUS_ERROR
    }

    private NELMediaMuxer mMediaMuxer;
    private Context mContext;
    private Listener mListener;
    private CountDownLatch mVideoTrackCountdownLatch;

    private HashMap<StreamType, VideoTrack> mVideoTracks;

    public NELVideoRecorder(Context context, Listener listener) {
        mContext = context;
        mListener = listener;
        mVideoTracks = new HashMap<>();
    }

    public void init() {
        mVideoTracks.clear();
    }

    public void uninit() {
        mContext = null;
        mListener = null;
        mVideoTracks.clear();
        mVideoTracks = null;
        System.gc();
    }

    public void addVideoTrack(StreamType streamType, String mimeType, int trackIndex, int width, int height, int bitRate, int frameRate, int rotateDegrees, int stackedFrames) {
        mVideoTracks.put(streamType, new VideoTrack(streamType, mimeType, trackIndex, width, height, bitRate, frameRate, rotateDegrees, stackedFrames));
    }

    public void start(String filePrefix, int videoOutputFormat) {
        mVideoTrackCountdownLatch = new CountDownLatch(mVideoTracks.size());
        String file = getVideoFilePath(filePrefix, videoOutputFormat);
        mMediaMuxer = new NELMediaMuxer(mContext, file, videoOutputFormat, mVideoTracks.size(), new NELMediaMuxer.Listener() {
            @Override
            public void onMuxerStarted() {
                sendRecordingProgressStarted();
            }
        });
        for (VideoTrack videoTrack : mVideoTracks.values()) {
            videoTrack.startEncoding();
        }
        sendRecordingStarted(file);
    }

    public void stop() {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                stop(VideoRecordingStatus.STATUS_OK);
            }
        });
    }

    private void stop(VideoRecordingStatus videoRecordingStatus) {
        try {
            Log.d(TAG, "stop");
            for (VideoTrack videoTrack : mVideoTracks.values()) {
                videoTrack.stopEncoding();
            }
            mVideoTrackCountdownLatch.await();
            Log.d(TAG, "All video tracks stopped");
            try {
                if (mMediaMuxer != null) {
                    mMediaMuxer.release();
                    mMediaMuxer = null;
                }
            }
            catch (Exception ex) {
                Log.e(TAG, "Failed in media muxer release", ex);
            }
            sendRecordingProgressStopped();
            Log.d(TAG, "Releasing video tracks");
            for (VideoTrack videoTrack : mVideoTracks.values()) {
                videoTrack.release();
            }
            Log.d(TAG, "All video tracks released");
            sendRecordingStopped(videoRecordingStatus);
        }
        catch (Exception ex) {
            Log.e(TAG, "Failed to stop video recording", ex);
            sendRecordingStopped(VideoRecordingStatus.STATUS_ERROR);
        }
    }

    private void sendRecordingStarted(String file) {
        if (mListener != null) {
            mListener.onVideoRecordingStarted(file);
        }
    }

    private void sendRecordingProgressStarted() {
        if (mListener != null) {
            mListener.onVideoRecordingProgressStarted();
        }
    }

    private void sendRecordingProgressStopped() {
        if (mListener != null) {
            mListener.onVideoRecordingProgressStopped();
        }
    }

    private void sendRecordingStopped(VideoRecordingStatus status) {
        if (mListener != null) {
            mListener.onVideoRecordingStopped(status);
        }
    }

    private String getVideoFilePath(String filePrefix, int videoOutputFormat) {
        final String directory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getAbsolutePath();
        final String extension = (videoOutputFormat == MediaMuxer.OutputFormat.MUXER_OUTPUT_MPEG_4) ? "mp4" : "mkv";
        final String timestamp = BuildConfig.DEBUG ? "dbg" : new SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault()).format(new Date());
        return Toolbox.formatString("%s%s%s_%s.%s", directory, File.separator, filePrefix, timestamp, extension);
    }

    private synchronized void notifyTrackDone() {
        if (mVideoTrackCountdownLatch != null) {
            mVideoTrackCountdownLatch.countDown();
        }
    }

    private class VideoTrack {
        private int mMuxerTrackIndex;
        private NELVideoEncoder mVideoEncoder;
        private NELVideoFrameCollector mVideoFrameCollector;
        private Thread mEncodingThread;
        private StreamType mStreamType;
        private String mMimeType;
        private boolean mIsRunning;
        private int mWidth;
        private int mHeight;
        private int mBitRate;
        private int mFrameRate;
        private int mRotateDegrees;
        private int mStackedFrames;

        VideoTrack(StreamType streamType, String mimeType, int trackIndex, int width, int height, int bitRate, int frameRate, int rotateDegrees, int stackedFrames) {
            mWidth = width;
            mHeight = height;
            mBitRate = bitRate;
            mFrameRate = frameRate;
            mRotateDegrees = rotateDegrees;
            mStackedFrames = stackedFrames;
            mIsRunning = false;
            mMuxerTrackIndex = trackIndex;
            mStreamType = streamType;
            mMimeType = mimeType;
            mEncodingThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    Log.d(TAG, String.format("Encoding thread START (%s)", mStreamType.name()));
                    VideoRecordingStatus status = initVideoEncoder();
                    if (status == VideoRecordingStatus.STATUS_OK) {
                        status = encodeVideo();
                        if (status != VideoRecordingStatus.STATUS_OK) {
                            stop(status);
                        }
                    }
                    else {
                        stop(status);
                    }
                    Log.d(TAG, String.format("Encoding thread STOP (%s)", mStreamType.name()));
                }
            });
        }

        void startEncoding() {
            Log.d(TAG, String.format("startEncoding (%s)", mStreamType.name()));
            mEncodingThread.setPriority(Thread.MAX_PRIORITY);
            mEncodingThread.start();
            mIsRunning = true;
        }

        void stopEncoding() {
            Log.d(TAG, String.format("stopEncoding (%s)", mStreamType.name()));
            if (!mIsRunning) {
                Log.d(TAG, "Stop already triggered");
            }
            mIsRunning = false;
        }

        void release() {
            Log.d(TAG, String.format("release (%s)", mStreamType.name()));
            mEncodingThread = null;
        }

        private VideoRecordingStatus initVideoEncoder() {
            Log.d(TAG, "initEncoder " + mStreamType.name());
            try {
                mVideoEncoder = new NELVideoEncoder(mMediaMuxer, mStreamType.name(), mMimeType, mMuxerTrackIndex, mBitRate, mFrameRate, mWidth, mHeight);
                mVideoFrameCollector = new NELVideoFrameCollector(mWidth, mHeight, mRotateDegrees, mStackedFrames);
                mVideoFrameCollector.setEncoderInputSurface(mVideoEncoder.getMediaCodec().createInputSurface());
                mVideoEncoder.getMediaCodec().start();
                mListener.onVideoFrameCollectorCreated(mVideoFrameCollector, mStreamType);
                mVideoFrameCollector.setVideoFrameCollectionEnabled(true);
                return VideoRecordingStatus.STATUS_OK;
            }
            catch (Exception ex) {
                Log.e(TAG, String.format("Failure in initVideoEncoder (%s)", mStreamType.name()), ex);
                return VideoRecordingStatus.STATUS_ERROR;
            }
        }

        private VideoRecordingStatus encodeVideo() {
            try {
                while (mIsRunning) {
                    VideoRecordingStatus status = handleVideoFrame();
                    if (status != VideoRecordingStatus.STATUS_OK) {
                        Log.e(TAG, "handleVideoFrame failed " + status.name());
                        mVideoEncoder.release();
                        mVideoFrameCollector.release();
                        mVideoEncoder = null;
                        return status;
                    }
                }
                mVideoFrameCollector.setVideoFrameCollectionEnabled(false);
                mVideoEncoder.drain(true);
                mVideoEncoder.release();
                mVideoFrameCollector.release();
                mVideoEncoder = null;
                notifyTrackDone();
                return VideoRecordingStatus.STATUS_OK;
            }
            catch (Exception ex) {
                Log.e(TAG, String.format("Failure in encodeVideo (%s)", mStreamType.name()), ex);
                mVideoEncoder.release();
                mVideoFrameCollector.release();
                mVideoEncoder = null;
                return VideoRecordingStatus.STATUS_ERROR;
            }
        }

        private VideoRecordingStatus handleVideoFrame() {
            try {
                if (!mIsRunning) {
                    return VideoRecordingStatus.STATUS_OK;
                }
                mVideoEncoder.drain(false);
                mVideoFrameCollector.waitForFrame();
                mVideoFrameCollector.drawVideoFrame();
                return VideoRecordingStatus.STATUS_OK;
            }
            catch (Exception ex) {
                if (!mIsRunning) {
                    return VideoRecordingStatus.STATUS_OK;
                }
                Log.e(TAG, String.format("Failure in handleVideoFrame (%s)", mStreamType.name()), ex);
                return VideoRecordingStatus.STATUS_ERROR;
            }
        }
    }

}
