package com.neuroeventlabs.rscamera.camera;

import android.graphics.Bitmap;

public interface CameraManagerListener {

    void onCameraAvailabilityChanged(boolean connected);
    void onRecordingStarted();
    void onRecordingProgress(int seconds);
    void onRecordingEnded(boolean cameraShutdownRequested);
    void onRecordingThumbnailAvailable(String recordingFile, Bitmap bitmap);

}
