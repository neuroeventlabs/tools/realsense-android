package com.neuroeventlabs.rscamera.videorecorder;

import android.content.Context;
import android.media.MediaCodec;
import android.media.MediaFormat;
import android.media.MediaMuxer;
import android.media.MediaScannerConnection;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import java.io.File;
import java.nio.ByteBuffer;

class NELMediaMuxer {

    private static final String TAG = "NELMediaMuxer";

    private String mFile;
    private Context mContext;
    private MediaMuxer mMediaMuxer;
    private MediaFormat[] mTrackFormats;
    private long mTrackSize[];
    private int mTracksAdded;
    private int mTotalNumberOfTracks;
    private boolean mIsReady;
    private Listener mListener;
    private Handler mMainHandler;

    interface Listener {
        void onMuxerStarted();
    }

    NELMediaMuxer(Context context, String file, int videoOutputFormat, int numberOfTracks, Listener listener) {
        try {
            mFile = file;
            removeExistingFile(mFile);
            mContext = context;
            mListener = listener;
            mIsReady = false;
            mTracksAdded = 0;
            mTotalNumberOfTracks = numberOfTracks;
            mTrackFormats = new MediaFormat[numberOfTracks];
            mMainHandler = new Handler(Looper.getMainLooper());
            mTrackSize = new long[numberOfTracks];
            for (int i = 0; i < numberOfTracks; i++) {
                mTrackSize[i] = 0;
            }
            mMediaMuxer = new MediaMuxer(file, videoOutputFormat);
        }
        catch (Exception ex) {
            Log.e(TAG, "Failure creating MediaMuxer", ex);
        }
    }

    synchronized void addTrack(int trackIndex, String trackIdentifier, MediaFormat mediaFormat) {
        Log.d(TAG, String.format("addTrack %s at index %d: %s", trackIdentifier, trackIndex, mediaFormat.toString()));
        mTrackFormats[trackIndex] = mediaFormat;
        mTracksAdded++;
        if (mTracksAdded == mTotalNumberOfTracks) {
            for (MediaFormat mf : mTrackFormats) {
                mMediaMuxer.addTrack(mf);
            }
            mMediaMuxer.start();
            Log.d(TAG, "Muxer started");
            mIsReady = true;
            sendOnMuxerStarted();
            notifyAll();
        }
        else {
            while (mTracksAdded < mTotalNumberOfTracks) {
                try {
                    wait();
                }
                catch (Exception e) {
                    Log.e(TAG, "Wait failed", e);
                }
            }
        }
    }

    void writeSampleData(int trackIndex, ByteBuffer byteBuf, MediaCodec.BufferInfo bufferInfo) {
        if (mMediaMuxer != null && mIsReady) {
            mMediaMuxer.writeSampleData(trackIndex, byteBuf, bufferInfo);
            mTrackSize[trackIndex] += bufferInfo.size;
        }
    }

    void release() {
        if (mMediaMuxer != null) {
            try {
                mMediaMuxer.stop();
                Log.d(TAG, "Muxer stopped");
            }
            catch (Exception e) {
                Log.e(TAG, "Muxer stopping failed", e);
            }
            mMediaMuxer.release();
            mIsReady = false;
            MediaScannerConnection.scanFile(mContext, new String[] { mFile }, new String[] { "video/mp4", "video/x-matroska" }, null);
            mContext = null;
        }
    }

    private void sendOnMuxerStarted() {
        if (mListener != null && mMainHandler != null) {
            mMainHandler.post(new Runnable() {
                @Override
                public void run() {
                    mListener.onMuxerStarted();
                }
            });
        }
    }

    private void removeExistingFile(String pathToVideoFile) {
        try {
            File file = new File(pathToVideoFile);
            if (file.exists()) {
                if (file.delete()) {
                    Log.d(TAG, "Removed existing video file");
                }
                else {
                    Log.e(TAG, "Failed to remove existing video file");
                }
            }
        }
        catch (Exception ex) {
            Log.e(TAG, "Failure in removeExistingFile", ex);
        }
    }
}
