package com.neuroeventlabs.rscamera.videorecorder;

import android.annotation.SuppressLint;
import android.graphics.Point;
import android.graphics.Rect;
import android.opengl.GLES20;
import android.opengl.Matrix;
import android.view.Surface;

import com.android.grafika.gles.EglCore;
import com.android.grafika.gles.FullFrameRect;
import com.android.grafika.gles.Texture2dProgram;
import com.android.grafika.gles.WindowSurface;
import com.intel.realsense.librealsense.GLVideoFrameNEL;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class NELVideoFrameCollector implements VideoFrameCollector {

    private static final String TAG = "NELVideoFrameCollector";
    private static final int TIMEOUT_MS     = 2500;

    private EglCore mEglCore;
    private WindowSurface mEncoderInputSurface;
    private FrameRenderer mFrameRenderer;
    private VideoFrame mVideoFrame;
    private HashMap<Integer, VideoFrame> mFrameMap;
    private final Object mFrameSyncObject = new Object();
    private boolean mFrameCollectionEnabled = false;
    private boolean mFrameAvailable = false;
    private long mKeyPresentationTime;
    private int mRotateDegrees;
    private int mStackedFrames;

    @SuppressLint("UseSparseArrays")
    NELVideoFrameCollector(int frameWidth, int frameHeight, int rotateDegrees, int stackedFrames) {
        mRotateDegrees = rotateDegrees;
        mStackedFrames = stackedFrames;
        mKeyPresentationTime = 0;
        mFrameMap = new HashMap<>();
        mFrameRenderer = new FrameRenderer(frameWidth, frameHeight);
        mEglCore = new EglCore(null, EglCore.FLAG_RECORDABLE);
    }

    void setEncoderInputSurface(Surface surface) {
        mEncoderInputSurface = new WindowSurface(mEglCore, surface, true);
    }

    void setVideoFrameCollectionEnabled(boolean isEnabled) {
        mFrameCollectionEnabled = isEnabled;
    }

    void release() {
        mEncoderInputSurface.release();
        mFrameMap.clear();
        mFrameMap = null;
        mVideoFrame.mFrame.close();
        mVideoFrame = null;
        mFrameRenderer.close();
        mFrameRenderer = null;
        mEglCore.release();
        mEglCore = null;
    }

    void waitForFrame() {
        synchronized (mFrameSyncObject) {
            while (!mFrameAvailable) {
                try {
                    mFrameSyncObject.wait(TIMEOUT_MS);
                    if (!mFrameAvailable) {
                        throw new RuntimeException("Timed out");
                    }
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            mFrameAvailable = false;
        }
    }

    void drawVideoFrame() {
        if (mVideoFrame != null && mEncoderInputSurface != null) {
            mEncoderInputSurface.makeCurrent();
            mFrameRenderer.drawFrame();
            mEncoderInputSurface.setPresentationTime(mVideoFrame.mPresentationTime);
            mEncoderInputSurface.swapBuffers();
        }
    }

    @Override
    public void onVideoFrameAvailable(GLVideoFrameNEL videoFrame, double timeStamp, int index) {
        synchronized (mFrameSyncObject) {
            mVideoFrame = new VideoFrame(videoFrame, timeStamp, getPresentationTime(timeStamp));
            mFrameMap.put(index, mVideoFrame);
            mFrameAvailable = true;
            mFrameSyncObject.notifyAll();
        }
    }

    @Override
    public boolean isFrameCollectionEnabled() {
        return mFrameCollectionEnabled;
    }

    private long getPresentationTime(double timeStamp) {
        long presentationTime = System.nanoTime();
        if (mVideoFrame != null) {
            if (mKeyPresentationTime != 0 && mVideoFrame.mTimestamp == timeStamp) {
                long deltaNanos = System.nanoTime() - mKeyPresentationTime;
                presentationTime += deltaNanos;
            }
            else {
                mKeyPresentationTime = presentationTime;
            }
        }
        return presentationTime;
    }

    private static class VideoFrame {
        private GLVideoFrameNEL mFrame;
        private long mPresentationTime;
        private double mTimestamp;

        VideoFrame(GLVideoFrameNEL frame, double timestamp, long presentationTime) {
            mFrame = frame;
            mTimestamp = timestamp;
            mPresentationTime = presentationTime;
        }
    }

    private class FrameRenderer {

        private List<Rect> mVideoRects;
        private FullFrameRect mFullFrameRect;
        private float[] mDisplayProjectionMatrix;
        private boolean mSetupDone;

        FrameRenderer(int frameWidth, int frameHeight) {
            mSetupDone = false;
            mVideoRects = new ArrayList<>();
            int i = 0;
            for (int stackedFrame = 0; stackedFrame <  mStackedFrames; stackedFrame++) {
                Point size = frameWidth > frameHeight ? new Point(frameWidth / mStackedFrames, frameHeight) : new Point(frameWidth, frameHeight / mStackedFrames);
                Point pos = frameWidth > frameHeight ? new Point(i++ * size.x, 0) : new Point(0, i++ * size.y);
                mVideoRects.add(new Rect(pos.x, pos.y, pos.x + size.x, pos.y + size.y));
            }
        }

        void close() {
            mFullFrameRect.release(true);
            mFullFrameRect = null;
        }

        void drawFrame() {

            if (!mSetupDone) {
                runSetup();
            }

            GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
            GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);

            Matrix.setIdentityM(mDisplayProjectionMatrix, 0);
            Matrix.translateM(mDisplayProjectionMatrix, 0, 0f, 0f, 0f);
            if (mRotateDegrees != 0) {
                Matrix.rotateM(mDisplayProjectionMatrix, 0, mRotateDegrees, 0.0f, 0f, 1f);
            }
            Matrix.scaleM(mDisplayProjectionMatrix, 0, -1f, 1f, 1f);

            for (int i = 0; i < mVideoRects.size(); i++) {
                if (mFrameMap.containsKey(i)) {
                    VideoFrame frame = mFrameMap.get(i);
                    if (frame != null) {
                        int texture1 = frame.mFrame.createTextureForRecording(mVideoRects.get(i));
                        if (texture1 < 0) {
                            return;
                        }
                        mFullFrameRect.drawFrame(texture1, mDisplayProjectionMatrix);
                    }
                }
            }
        }

        private void runSetup() {
            mDisplayProjectionMatrix = new float[16];
            GLES20.glDisable(GLES20.GL_CULL_FACE);
            mFullFrameRect = new FullFrameRect(new Texture2dProgram(Texture2dProgram.ProgramType.TEXTURE_2D));
            mFullFrameRect.createTextureObject();
            mSetupDone = true;
        }
    }
}

