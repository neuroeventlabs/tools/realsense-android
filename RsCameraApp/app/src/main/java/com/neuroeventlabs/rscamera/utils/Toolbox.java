package com.neuroeventlabs.rscamera.utils;

import java.io.File;
import java.util.Locale;

public class Toolbox {

    public static String formatString(String format, Object... args) {
        return String.format(Locale.getDefault(), format, args);
    }

    public static boolean fileExists(String path) {
        try {
            return new File(path).exists();
        }
        catch (Exception ex) {
            return false;
        }
    }

}
