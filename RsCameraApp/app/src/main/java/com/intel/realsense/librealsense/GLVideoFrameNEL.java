/*
    Copyright 2017 Intel Corporation

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this project except in compliance with the License.
    You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

// https://github.com/IntelRealSense/librealsense/blob/master/LICENSE

/*
    Derivative work of com.intel.realsense.librealsense.GLVideoFrame

    Modifications Copyright(c) 2019 Neuro Event Labs

    Modification: Name changed GLVideoFrame -> GLVideoFrameNEL to avoid clash with original GLVideoFrame in .aar
    Modification: Extends modified GLFrameNEL instead of original GLFrame
    Modification: Added GLES20 based code for generating video frame texture for video recording
    Modification: Added rotate boolean parameter to draw methods
*/

package com.intel.realsense.librealsense;

import android.graphics.Rect;
import android.opengl.GLES10;
import android.opengl.GLES20;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;

public class GLVideoFrameNEL extends GLFrameNEL {
    private IntBuffer mGlTexture;

    public int getTexture() { return mGlTexture.array()[0]; }

    private Rect adjustRatio(Rect in){
        if (mFrame == null || !(mFrame instanceof VideoFrame))
            return null;

        VideoFrame vf = mFrame.as(VideoFrame.class);

        float ratio = (float)vf.getWidth() / (float)vf.getHeight();
        float newHeight = in.height();
        float newWidth = in.height() * ratio;
        if(newWidth > in.width()){
            ratio = in.width() / newWidth;
            newWidth *= ratio;
            newHeight *= ratio;
        }

        float newLeft = in.left + (in.width() - newWidth) / 2f;
        float newTop = in.top + (in.height() - newHeight) / 2f;
        float newRight = newLeft + newWidth;
        float newBottom = newTop + newHeight;
        return new Rect((int)newLeft, (int)newTop, (int)newRight, (int)newBottom);
    }

    @Override
    public synchronized void draw(Rect rect, boolean rotate)
    {
        if (mFrame == null || !(mFrame instanceof VideoFrame))
            return;

        if(mGlTexture == null) {
            mGlTexture = IntBuffer.allocate(1);
            GLES10.glGenTextures(1, mGlTexture);
        }

        VideoFrame vf = mFrame.as(VideoFrame.class);
        int size = vf.getStride() * vf.getHeight();
        if(mBuffer == null || mBuffer.array().length != size){
            mBuffer = ByteBuffer.allocate(size);
            mBuffer.order(ByteOrder.LITTLE_ENDIAN);
        }

        mFrame.getData(mBuffer.array());
        mBuffer.rewind();

        upload(vf, mBuffer, mGlTexture.get(0));
        Rect r = adjustRatio(rect);
        draw(r, mGlTexture.get(0), rotate);
    }

    @Override
    public synchronized void close() {
        if(mFrame != null)
            mFrame.close();
        if(mGlTexture != null)
            GLES10.glDeleteTextures(1, mGlTexture);
        mGlTexture = null;
    }

    public static void upload(VideoFrame vf, ByteBuffer buffer, int texture)
    {
        GLES10.glBindTexture(GLES10.GL_TEXTURE_2D, texture);

        switch (vf.getProfile().getFormat())
        {
            case RGB8:
            case BGR8:
                GLES10.glTexImage2D(GLES10.GL_TEXTURE_2D, 0, GLES10.GL_RGB, vf.getWidth(), vf.getHeight(), 0, GLES10.GL_RGB, GLES10.GL_UNSIGNED_BYTE, buffer);
                break;
            case RGBA8:
                GLES10.glTexImage2D(GLES10.GL_TEXTURE_2D, 0, GLES10.GL_RGBA, vf.getWidth(), vf.getHeight(), 0, GLES10.GL_RGBA, GLES10.GL_UNSIGNED_BYTE, buffer);
                break;
            case Y8:
                GLES10.glTexImage2D(GLES10.GL_TEXTURE_2D, 0, GLES10.GL_LUMINANCE, vf.getWidth(), vf.getHeight(), 0, GLES10.GL_LUMINANCE, GLES10.GL_UNSIGNED_BYTE, buffer);
                break;
            default:
                throw new RuntimeException("The requested format is not supported by the viewer");
        }

        GLES10.glTexParameterx(GLES10.GL_TEXTURE_2D, GLES10.GL_TEXTURE_MAG_FILTER, GLES10.GL_LINEAR);
        GLES10.glTexParameterx(GLES10.GL_TEXTURE_2D, GLES10.GL_TEXTURE_MIN_FILTER, GLES10.GL_LINEAR);
        GLES10.glTexParameterx(GLES10.GL_TEXTURE_2D, GLES10.GL_TEXTURE_WRAP_S, 0x2900);
        GLES10.glTexParameterx(GLES10.GL_TEXTURE_2D, GLES10.GL_TEXTURE_WRAP_T, 0x2900);
        GLES10.glPixelStorei(0x0CF2, 0);
        GLES10.glBindTexture(GLES10.GL_TEXTURE_2D, 0);
    }

    public static void draw(Rect r, int texture, boolean rotate)
    {
        setViewport(r, rotate);

        GLES10.glBindTexture(GLES10.GL_TEXTURE_2D, texture);
        GLES10.glEnable(GLES10.GL_TEXTURE_2D);

        float[] verArray = {
                0,          0,
                0,          r.height(),
                r.width(),  r.height(),
                r.width(),   0
        };
        ByteBuffer ver = ByteBuffer.allocateDirect(verArray.length * 4);
        ver.order(ByteOrder.nativeOrder());

        float[] texArray = {
                0,0,
                0,1,
                1,1,
                1,0
        };
        ByteBuffer tex = ByteBuffer.allocateDirect(texArray.length * 4);
        tex.order(ByteOrder.nativeOrder());

        ver.asFloatBuffer().put(verArray);
        tex.asFloatBuffer().put(texArray);

        GLES10.glEnableClientState(GLES10.GL_VERTEX_ARRAY);
        GLES10.glEnableClientState(GLES10.GL_TEXTURE_COORD_ARRAY);

        GLES10.glVertexPointer(2, GLES10.GL_FLOAT, 0, ver);
        GLES10.glTexCoordPointer(2, GLES10.GL_FLOAT, 0, tex);
        GLES10.glDrawArrays(GLES10.GL_TRIANGLE_FAN,0,4);

        GLES10.glDisableClientState(GLES10.GL_VERTEX_ARRAY);
        GLES10.glDisableClientState(GLES10.GL_TEXTURE_COORD_ARRAY);

        GLES10.glDisable(GLES10.GL_TEXTURE_2D);
        GLES10.glBindTexture(GLES10.GL_TEXTURE_2D, 0);
    }

    // Neuro Event Labs modification START
    public synchronized int createTextureForRecording(Rect rect) {
        if (mFrame == null || !(mFrame instanceof VideoFrame)) {
            return -1;
        }

        if (mGlTexture == null) {
            mGlTexture = IntBuffer.allocate(1);
            GLES20.glGenTextures(1, mGlTexture);
        }

        VideoFrame vf = mFrame.as(VideoFrame.class);
        int size = vf.getStride() * vf.getHeight();
        if (mBuffer == null || mBuffer.array().length != size){
            mBuffer = ByteBuffer.allocate(size);
            mBuffer.order(ByteOrder.LITTLE_ENDIAN);
        }

        mFrame.getData(mBuffer.array());
        mBuffer.rewind();

        uploadForRecording(vf, mBuffer, mGlTexture.get(0));
        GLES20.glViewport(rect.left, rect.top, rect.width(), rect.height());
        return mGlTexture.get(0);
    }

    private static void uploadForRecording(VideoFrame vf, ByteBuffer buffer, int texture)
    {
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, texture);

        switch (vf.getProfile().getFormat())
        {
            case RGB8:
            case BGR8:
                GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGB, vf.getWidth(), vf.getHeight(), 0, GLES20.GL_RGB, GLES20.GL_UNSIGNED_BYTE, buffer);
                break;
            case RGBA8:
                GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGBA, vf.getWidth(), vf.getHeight(), 0, GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, buffer);
                break;
            case Y8:
                GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_LUMINANCE, vf.getWidth(), vf.getHeight(), 0, GLES20.GL_LUMINANCE, GLES20.GL_UNSIGNED_BYTE, buffer);
                break;
            default:
                throw new RuntimeException("The requested format is not supported by the viewer");
        }

        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_REPEAT);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_REPEAT);
        GLES20.glPixelStorei(0x0CF2, 0);

        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
    }
    // Neuro Event Labs modification END
}
