/*
    Copyright 2017 Intel Corporation

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this project except in compliance with the License.
    You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

// https://github.com/IntelRealSense/librealsense/blob/master/LICENSE

/*
    Derivative work of com.intel.realsense.librealsense.GLFrame

    Modifications Copyright(c) 2019 Neuro Event Labs

    Modification: Name changed GLFrame -> GLFrameNEL
    Modification: Add parameter for rotation (if true, the frame is rotated -90 degrees)
*/

package com.intel.realsense.librealsense;

import android.graphics.Rect;
import android.opengl.GLES10;

import java.nio.ByteBuffer;

public abstract class GLFrameNEL implements AutoCloseable {
    protected Frame mFrame;
    protected ByteBuffer mBuffer;

    public abstract void draw(Rect rect, boolean rotate);

    public synchronized void setFrame(Frame frame) {
        if(mFrame != null)
            mFrame.close();
        mFrame = frame.clone();
    }

    protected static void setViewport(Rect r, boolean rotate) {
        GLES10.glViewport(r.left, r.top, r.width(), r.height());
        GLES10.glLoadIdentity();
        GLES10.glMatrixMode(GLES10.GL_PROJECTION);
        // Neuro Event Labs modification START
        if (rotate) {
            GLES10.glRotatef(-90, 0, 0, 1);
        }
        // Neuro Event Labs modification END
        GLES10.glOrthof(0, r.width(), r.height(), 0, -1, +1);
    }
}
