/*
    Copyright 2017 Intel Corporation

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this project except in compliance with the License.
    You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

// https://github.com/IntelRealSense/librealsense/blob/master/LICENSE

/*
    Derivative work of com.intel.realsense.librealsense.GLRsSurfaceView

    Modifications Copyright(c) 2019 Neuro Event Labs

    Modification: Name changed GLRsSurfaceView -> GLRsSurfaceViewNEL to avoid clash with GLRsSurfaceView in .aar
    Modification: Use modified GLRenderer (GLRendererNEL)
    Modification: Add getter for renderer
    Modification: setStreamCount method added
    Modification: setRecordingEnabled method added
*/

package com.intel.realsense.librealsense;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;

public class GLRsSurfaceViewNEL extends GLSurfaceView {

    private final GLRendererNEL mRenderer;

    public GLRsSurfaceViewNEL(Context context) {
        super(context);
        mRenderer = new GLRendererNEL();
        setRenderer(mRenderer);
    }

    public GLRsSurfaceViewNEL(Context context, AttributeSet attrs) {
        super(context, attrs);
        mRenderer = new GLRendererNEL();
        setRenderer(mRenderer);
    }

    public GLRendererNEL getRenderer() {
        return mRenderer;
    }

    public void upload(FrameSet frames) {
        mRenderer.upload(frames);
    }

    public void upload(Frame frame) {
        mRenderer.upload(frame);
    }

    public void clear() {
        mRenderer.clear();
    }

    public void setRecordingEnabled(boolean recordingEnabled) {
        mRenderer.setRecordVideoEnabled(recordingEnabled);
    }

    public void setStreamCount(int streamCount) {
        mRenderer.setStreamCount(streamCount);
    }
}
