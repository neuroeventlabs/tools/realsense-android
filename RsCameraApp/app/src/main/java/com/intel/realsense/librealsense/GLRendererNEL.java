/*
    Copyright 2017 Intel Corporation

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this project except in compliance with the License.
    You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

// https://github.com/IntelRealSense/librealsense/blob/master/LICENSE

/*
    Derivative work of com.intel.realsense.librealsense.GLRenderer

    Modifications Copyright(c) 2019 Neuro Event Labs

    Modification: Name changed GLRenderer -> GLRendererNEL to avoid clash with GLRenderer in .aar
    Modification: Use modified GLFrameNEL/GLVideoFrameNEL instead of GLFrame/GLVideoFrame from .aar
    Modification: onDrawFrame modified to draw full screen portrait and collect frame for video recording
    Modification: onDrawFrame modified to handle video recording event when multiple streams are stacked on top of each other
    Modification: setStreamCount method added
    Modification: setVideoFrameCollector method added (for recording)
    Modification: setRecordVideoEnabled method added
*/

package com.intel.realsense.librealsense;

import android.graphics.Point;
import android.graphics.Rect;
import android.opengl.GLES10;
import android.opengl.GLSurfaceView;

import com.neuroeventlabs.rscamera.videorecorder.VideoFrameCollector;

import java.util.HashMap;
import java.util.Map;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class GLRendererNEL implements GLSurfaceView.Renderer {

    private final Map<Integer, GLFrameNEL> mFrames = new HashMap<>();
    private int mWindowHeight = 0;
    private int mWindowWidth = 0;
    private boolean mIsDirty = true;
    private boolean mRecordVideo = false;
    private int mStreamCount = 1;

    private VideoFrameCollector mVideoFrameCollector;

    public void setVideoFrameCollector(VideoFrameCollector videoFrameCollector) {
        mVideoFrameCollector = videoFrameCollector;
    }

    void setRecordVideoEnabled(boolean recordVideo) {
        mRecordVideo = recordVideo;
    }

    void setStreamCount(int streamCount) {
        mStreamCount = streamCount;
    }

    public void upload(FrameSet frameSet) {
        frameSet.foreach(new FrameCallback() {
            @Override
            public void onFrame(Frame f) {
                upload(f);
            }
        });
    }

    public void upload(Frame f) {
        if(f == null)
            return;

        if(!isFormatSupported(f.getProfile().getFormat()))
            return;

        int uid = f.getProfile().getUniqueId();
        if(!mFrames.containsKey(uid)){
            synchronized (mFrames) {
                mFrames.put(uid, new GLVideoFrameNEL());
            }
            mIsDirty = true;
        }
        mFrames.get(uid).setFrame(f);
    }

    public void clear() {
        synchronized (mFrames) {
            mFrames.clear();
        }
        mIsDirty = true;
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {

    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        mWindowWidth = width;
        mWindowHeight = height;
        mIsDirty = true;
    }

    @Override
    public void onDrawFrame(GL10 gl) {
        synchronized (mFrames) {
            if (mIsDirty) {
                GLES10.glViewport(0, 0, mWindowWidth, mWindowHeight);
                GLES10.glClearColor(0, 0, 0, 1);
                mIsDirty = false;
            }

            if (mFrames.size() == 0)
                return;

            if (mStreamCount == 1) {
                Rect r = new Rect(0, mWindowHeight, mWindowWidth, 0);

                for (Map.Entry<Integer, GLFrameNEL> entry : mFrames.entrySet()) {

                    GLVideoFrameNEL fl = (GLVideoFrameNEL) entry.getValue();

                    if (!mRecordVideo) {
                        fl.draw(r, true);
                    } else {
                        if (mVideoFrameCollector != null && mVideoFrameCollector.isFrameCollectionEnabled()) {
                            mVideoFrameCollector.onVideoFrameAvailable(fl, fl.mFrame.getTimestamp(), 0);
                        }
                    }
                }
            }
            else {
                int i = 0;
                for (Map.Entry<Integer, GLFrameNEL> entry : mFrames.entrySet()) {
                    int index = i;
                    Point size = mWindowWidth > mWindowHeight ? new Point(mWindowWidth / mFrames.size(), mWindowHeight) : new Point(mWindowWidth, mWindowHeight / mFrames.size());
                    Point pos = mWindowWidth > mWindowHeight ? new Point(i++ * size.x, 0) : new Point(0, i++ * size.y);

                    Rect r = new Rect(pos.x, pos.y, pos.x + size.x, pos.y + size.y);
                    GLVideoFrameNEL fl = (GLVideoFrameNEL) entry.getValue();

                    if (!mRecordVideo) {
                        fl.draw(r, false);
                    }
                    else {
                        if (mVideoFrameCollector != null && mVideoFrameCollector.isFrameCollectionEnabled()) {
                            mVideoFrameCollector.onVideoFrameAvailable(fl, fl.mFrame.getTimestamp(), index);
                        }
                    }
                }
            }
        }
    }

    private boolean isFormatSupported(StreamFormat format) {
        switch (format){
            case RGB8:
            case RGBA8:
            case Y8: return true;
            default: return false;
        }
    }
}