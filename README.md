# RS Recorder

An Android application for capturing streams from a Realsense camera.

Includes app specific modifications to RealSense Android wrappers (derivative work based on the RealSense SDK 2.0 version 2.19.2)
https://github.com/IntelRealSense/librealsense/tree/v2.19.2

Includes GLES code from Grafika project (based on commit 60d36bef7f4960c631be3b73b17b69f86cc6600f)
https://github.com/google/grafika
